class generator;
//------------------------------------------------------------------------------
mailbox gen2driv;

transaction tr;
//------------------------------------------------------------------------------
task automatic main();
   tr = new();
   void'(tr.randomize());
   if(!tr.randomize()) 
      $fatal("randomization::failed");
   gen2driv.put(tr);
   tr.display(" G E N E R A T O R ");
endtask
//------------------------------------------------------------------------------
function new(mailbox gen2driv);
   this.gen2driv = gen2driv;
endfunction
//------------------------------------------------------------------------------
endclass
