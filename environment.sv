class environment;
 
 virtual intf vif;
 mailbox gen2driv;
 mailbox driv2sco;
 mailbox mon2sco;
 
 generator gen; 
 driver driv;
 monitor mon;
 scoreboard sco;
 
//------------------------------------------------------------------------------
task run();
    fork
      gen.main();
      driv.main();
      mon.main();
      sco.main(); 
    join
endtask
//------------------------------------------------------------------------------
task reset();
   driv.reset();
endtask
//------------------------------------------------------------------------------
function new(virtual intf vif);
   this.vif = vif;

   gen2driv = new();
   mon2sco = new();
   driv2sco = new();
   
   gen = new(.gen2driv(gen2driv));
   driv = new(.vif(vif),.gen2driv(gen2driv),.driv2sco(driv2sco));
   mon = new(.vif(vif),.mon2sco(mon2sco));
   sco = new(.driv2sco(driv2sco),.mon2sco(mon2sco));

endfunction
endclass
