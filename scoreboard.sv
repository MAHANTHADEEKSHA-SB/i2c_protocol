class scoreboard;

mailbox mon2sco;

mailbox driv2sco;
//------------------------------------------------------------------------------
task automatic main();
   transaction tr_mon;
   transaction tr_driv;
   mon2sco.get(tr_mon);
   driv2sco.get(tr_driv);


endtask
//------------------------------------------------------------------------------
function new(mailbox mon2sco,mailbox driv2sco);
     this.mon2sco = mon2sco;
     this.driv2sco = driv2sco;
endfunction
endclass
