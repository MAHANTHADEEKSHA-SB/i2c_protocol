class monitor;

virtual intf vif;

mailbox mon2sco;
//------------------------------------------------------------------------------
task automatic main();
   int i;
   transaction tr;
      tr = new();
   forever begin
     for(i = 0; i < 8 ; i = i + 1)  begin
        @(posedge vif.i2c_scl) tr.data[i] = vif.i2c_sda;
     end
     wait(vif.ready) break;
   end
   $display("@time-> %0t data received %0b",$time(),tr.data);
   mon2sco.put(tr);
endtask
//------------------------------------------------------------------------------
function new(virtual intf vif,mailbox mon2sco);
    this.vif = vif;
    this.mon2sco = mon2sco;
endfunction

endclass
