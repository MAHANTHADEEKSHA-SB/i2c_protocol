interface intf(input logic clock);

   logic rst;
   logic [6:0] addr;
   logic [7:0] data_in;
   logic enable;
   logic rw;
   logic [7:0] data_out;
   logic ready;
   wire i2c_sda;
   wire i2c_scl;
//------------------------------------------------------------------------------
clocking cb @(posedge clock);
    default input #1 output #1;
    output clock;
    output addr;
    output data_in;
    output enable;
    output rw;
    input data_out;
    input ready;
    inout i2c_sda;
    inout i2c_scl;
endclocking
//------------------------------------------------------------------------------
modport mb (clocking cb,output rst,input clock);
//------------------------------------------------------------------------------
endinterface
