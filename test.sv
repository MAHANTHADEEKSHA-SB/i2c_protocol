program test(intf intf_);
  environment env;
  initial begin
     env = new(intf_);
     env.reset();
     env.run();
  end
endprogram
