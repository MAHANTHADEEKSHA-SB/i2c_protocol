`timescale 1ns / 1ps
`include "i2c_master.v"
`include "i2c_slave.v"
`include "interface.sv"
`include "transaction.sv"
`include "generator.sv"
`include "driver.sv"
`include "monitor.sv"
`include "scoreboard.sv"
`include "environment.sv"
`include "test.sv"

module i2c_controller_tb;

    bit clock;
    initial begin
       clock = 1;
    end
 
    always #5 clock = ~clock;

    intf interface_(.clock(clock));
  
    test U_test(.intf_(interface_));
	
    i2c_controller master (
		.clk(interface_.clock), 
		.rst(interface_.rst), 
		.addr(interface_.addr), 
		.data_in(interface_.data_in), 
		.enable(interface_.enable), 
		.rw(interface_.rw), 
		.data_out(interface_.data_out), 
		.ready(interface_.ready), 
		.i2c_sda(interface_.i2c_sda), 
		.i2c_scl(interface_.i2c_scl)
	);
	
		
	i2c_slave_controller slave (
    .sda(interface_.i2c_sda), 
    .scl(interface_.i2c_scl)
    );

  initial begin
   #2000 $finish();
  end 
endmodule
