class driver;

virtual intf vif;

mailbox gen2driv;

mailbox driv2sco;
//------------------------------------------------------------------------------
task automatic main();
  begin
    transaction tr;
    $display("               |||||||||||| [ D R I V E R ] ||||||||||||");
    gen2driv.get(tr);
    wait(vif.ready);//wait for ready before sending data
    send_addr(tr);//address sending
    send_mode(tr);//mode bit sending
    send_data(tr);//data sending
    driv2sco.put(tr);
    $display("                                         ");
  end
endtask
//------------------------------------------------------------------------------
task automatic send_addr(transaction tr);//this task sends the address
   begin
       //@(posedge vif.clock);
       vif.addr <= tr.addr;
       $display("@time->%0t ||||||||||||---addr driven[ %0b  ]---|||||||||||",$time(),tr.addr);
   end
endtask
//------------------------------------------------------------------------------
task automatic send_mode(transaction tr);//this task sends the mode bit
   begin
      vif.rw <= tr.rw;
      $display("@time->%0t ||||||||||||---r/w sent[ %0b ]---||||||||||||",$time(),tr.rw);
   end
endtask
//------------------------------------------------------------------------------
task automatic send_data(transaction tr);//this task sends the data
    begin
      vif.data_in <= tr.data;
      $display("@time->%0t ||||||||||||---data driven[ %0d ]---||||||||||||",$time(),tr.data);
    end
endtask
//------------------------------------------------------------------------------
task reset();//this is the reset task
   $display("@time->%0t ~~~~~~~~~~~~~~~~~~[ R E S E T I N G ]~~~~~~~~~~~~~~~~~",$time());
   vif.rst <= 0;
   @(posedge vif.clock) vif.rst <= 1;
   repeat(100) @(posedge vif.clock);
   @(posedge vif.clock) vif.rst <= 0;
   vif.enable <= 1;
   $display("@time-> %0t ~~~~~~~~~~~~~~~~~~~~~[ R E S E T___F I N I S H E D ]~~~~~~~~~~~~~~~~~~~~",$time());
endtask
//------------------------------------------------------------------------------
function new(mailbox gen2driv,mailbox driv2sco,virtual intf vif);//driver class arguments
    this.gen2driv = gen2driv;
    this.driv2sco = driv2sco;
    this.vif = vif;
endfunction
//------------------------------------------------------------------------------
endclass
