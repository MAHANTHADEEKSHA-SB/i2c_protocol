class transaction;
//------------------------------------------------------------------------------
rand bit [6:0] addr;
rand bit [7:0] data;
rand bit rw;
bit [7:0] data_out;
bit ready;
bit i2c_sda;
bit i2c_scl;
//------------------------------------------------------------------------------
constraint c1 { addr == 7'b0101010; };
//------------------------------------------------------------------------------
function void display(string s);
    $display("@time->%0t #->->->-> [%s]<-<-<-<-<-#",$time(),s);
    $display("The field values are addr %0b data %0d mode %0b",addr,data,rw);
    $display("                                      ");
endfunction
//------------------------------------------------------------------------------
function new();
endfunction
//------------------------------------------------------------------------------
endclass
